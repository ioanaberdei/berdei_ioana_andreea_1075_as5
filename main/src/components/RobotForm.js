import React from 'react';

class RobotForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            type: '',
            mass: ''
        }
    }
    
    handleNameChange = event => {
        this.setState({
            name: event.target.value
        })
    }
    
    handleTypeChange = event => {
        this.setState({
            type: event.target.value
        })
    }
    
    handleMassChange = event => {
        this.setState({
            mass: event.target.value
        })
    }
    
    render() {
        return (
            <div>
                <form>
                    <input type="text" id="name" placeholder="name" onChange={this.handleNameChange}/>
                    <input type="text" id="type" placeholder="type" onChange={this.handleTypeChange}/>
                    <input type="text" id="mass" placeholder="mass" onChange={this.handleMassChange}/>
                    <input type="button" value="add" onClick={()=>this.props.onAdd({
                        name:this.state.name,
                        type:this.state.type,
                        mass:this.state.mass
                    })}/>
                </form>
            </div>
        )
    }
}

export default RobotForm;